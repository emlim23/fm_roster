// var mysql = require("mysql")
var pg = require('pg')
	, config = require('../config')
  , connectionString = process.env.HEROKU_POSTGRESQL_PURPLE_URL || ("pg://"+config.DB_USER+":"+config.DB_PW+"@"+config.DATABASE_URL+"/"+config.DB_CAT);
	// , connInitDB = mysql.createConnection({
	// 			host     : config.DATABASE_URL,
	// 			user     : config.DB_USER,
	// 			password : config.DB_PW
	// });
	// , pool  = mysql.createPool({
	// 					  host     : config.DATABASE_URL,
	// 					  user     : config.DB_USER,
	// 					  password : config.DB_PW,
	// 					});

var createDB = function(response) {
	// pool.getConnection(function(err, connection) {
  // Use the connection
  var createStr = 'CREATE TABLE roster(id SERIAL PRIMARY KEY, name VARCHAR(100), status VARCHAR(100) NOT NULL, ispresent INT NOT NULL);';
  pg.connect(connectionString, function(err, client, done) {
    client.query(createStr, function(err, result) {
      if(err){
        response.send(err);
      }
      else{
        response.send("Database and table created!");
      }
      done();
    });
  });
  // connInitDB.query( 'use ' + config.DB_CAT, function(err, rows) {
  //   // And done with the connection.

  //   if(err){
  //   	if(err.code == "ER_BAD_DB_ERROR"){
  //   		var 
  //   				errCB = function(err){
	 //    				response.send(err);
	 //    			}
  //   			, tableCB = function(rows){
  //   					response.send("database and tables created!");
  //   				}
  //   			, cb = function(rows){
  //   					createTables(tableCB, errCB);
  //   					connInitDB.destroy();
		//     		}

  //   		buildDB(cb, errCB);
  //   	}
  //   	else{
  //   		response.send(err);
  //   	}
  //   }
  //   else{
  //   	response.send("Database and tables already exists!");
  //   }
  // });
	// });
}

function buildDB(cb, errCB) {
  connInitDB.query( 'CREATE DATABASE IF NOT EXISTS ' + config.DB_CAT, function(err, rows) {
  	if(err)
    	errCB(err);
    else
    	cb(rows);    
  });
}

function createTables(cb, errCB) {
	var connInitTables = mysql.createConnection({
				host     : config.DATABASE_URL,
				user     : config.DB_USER,
				password : config.DB_PW,
				database : config.DB_CAT
	})
	var createStr = 'CREATE TABLE roster(id INT NOT NULL AUTO_INCREMENT, name VARCHAR(100) NOT NULL, status VARCHAR(100) NOT NULL, isPresent INT NOT NULL, PRIMARY KEY ( id ) );';
	// var createStr = 'USE '+config.DB_CAT+';';
  
  connInitTables.query( createStr, function(err, rows) {
  	if(err)
    	errCB(err);
    else
    	cb(rows);    

    connInitTables.destroy();
  });
}

exports.createDB = createDB;