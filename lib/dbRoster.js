var pg = require('pg')
  , config = require('../config')
  , connectionString = process.env.HEROKU_POSTGRESQL_PURPLE_URL || ("pg://"+config.DB_USER+":"+config.DB_PW+"@"+config.DATABASE_URL+"/"+config.DB_CAT)
  , roster = {}
  , rosterTable = 'roster';

roster.list = function(callback, errCB) {
  pg.connect(connectionString, function(err, connection, done) {
  // pool.getConnection(function(err, connection) {
    // Use the connection
    console.log(err);
    connection.query( 'SELECT * FROM '+rosterTable + ' ORDER BY name', function(err, result) {
      // And done with the connection.
      var rows = result.rows;
      if(err)
        errCB(err);
      else{
        var rosterList = []
          , ros;

        for(var i=0; i<rows.length; i++){
          rosterList.push({id:rows[i].id, name:rows[i].name, status:rows[i].status, isPresent: (rows[i].ispresent == 1)});
        }
        callback(rosterList);
      }

      done();
    });
  });
}

roster.get = function(id, callback, errCB) {
  // pool.getConnection(function(err, connection) {
  //   // Use the connection
  //   connection.query( 'SELECT * FROM '+rosterTable + ' WHERE id = '+id, function(err, rows) {
  //     // And done with the connection.
  //     if(err)
  //       errCB(err);
  //     else
  //       callback(rows);

  //     connection.release();
  //   });
  // });
}

roster.save = function(rosterData, callback, errCB) {
  pg.connect(connectionString, function(err, connection, done) {
    var sql = ""
      , is_present = (rosterData.isPresent) ? 1:0;

    if(rosterData.id == 0){
      sql = "INSERT INTO " + rosterTable + "(name, status, ispresent) VALUES('"+rosterData.name+"', '"+rosterData.status+"', "+is_present+") RETURNING id";
    }
    else{
      sql = "UPDATE " + rosterTable + " SET name='"+rosterData.name+"', status='"+rosterData.status+"', ispresent="+is_present+") WHERE id = "+rosterData.id;
    }

    connection.query( sql, function(err, result) {
      // And done with the connection.
      if(err)
        errCB(err);
      else
        callback(result.rows[0].id);

      done();
    });
  });
}

roster.setNewActive = function(data, callback, errCB) {
  pg.connect(connectionString, function(err, connection, done) {
    var sql = "UPDATE " + rosterTable + " SET status='done' WHERE status = 'active'; UPDATE " + rosterTable + " SET status='active' WHERE id = "+data.data.id;

    connection.query( sql, function(err, result) {
      // And done with the connection.
      if(err)
        errCB(err);
      else{
        callback(data);

        done();
      }
    });
  });
}

roster.setInActive = function(data, callback, errCB) {
  pg.connect(connectionString, function(err, connection, done) {
    var sql = "UPDATE " + rosterTable + " SET status='default' WHERE id = "+data.data.id;

    connection.query( sql, function(err, result) {
      // And done with the connection.
      if(err)
        errCB(err);
      else{
        callback(data);

        done();
      }
    });
  });
}

roster.setPresentStatus = function(data, callback, errCB) {
  pg.connect(connectionString, function(err, connection, done) {
    var sql = "UPDATE " + rosterTable + " SET ispresent='"+(data.data.status)+"' WHERE id = "+data.data.id;

    connection.query( sql, function(err, result) {
      // And done with the connection.
      if(err)
        errCB(err);
      else{
        callback(data);

        done();
      }
    });
  });
}

roster.reset = function(data, callback, errCB) {
  pg.connect(connectionString, function(err, connection, done) {
    var sql = "UPDATE " + rosterTable + " SET status='default'";

    connection.query( sql, function(err, rows) {
      // And done with the connection.
      if(err)
        errCB(err);
      else{
        callback(data);

        done();
      }
    });
  });
}

roster.updatePresent = function(data, callback, errCB) {
  pg.connect(connectionString, function(err, connection, done) {
    var sqlPresents = "UPDATE " + rosterTable + " SET ispresent=1 WHERE id IN ("
      , sqlAbsents = "UPDATE " + rosterTable + " SET ispresent=0 WHERE id IN ("
      , presentIDs = []
      , absentIDs = []
      , sql = []; 

    //build the sql statements first
    for(var i2=0; i2<data.data.length; i2++){
      if(data.data[i2].isPresent)
        presentIDs.push(data.data[i2].id);
      else
        absentIDs.push(data.data[i2].id);
    }

    if(presentIDs.length > 0){
      sqlPresents += presentIDs.join(',') + ")";
      sql.push(sqlPresents);
    }
    
    if(absentIDs.length > 0){
      sqlAbsents += absentIDs.join(',') + ")";
      sql.push(sqlAbsents);
    }

    connection.query( sql.join(';'), function(err, rows) {
      // And done with the connection.
      if(err)
        errCB(err);
      else{
        callback(data);

        done();
      }
    });
  });
}

roster.delete = function(id, callback, errCB) {
  pg.connect(connectionString, function(err, connection, done) {
    var sql = "DELETE FROM " + rosterTable + " WHERE id = "+id;
    connection.query( sql, function(err, rows) {
      // And done with the connection.
      if(err)
        errCB(err);
      else
        callback(rows.affectedRows);

      done();
    });
  });
}

roster.updateFromList = function(list, callback, errCB) {
  if(list && list.length > 0){
    pg.connect(connectionString, function(err, connection, done) {
      var sqlInsert = "INSERT INTO "+rosterTable+" (name, status, ispresent) VALUES "
        , inserts = []
        , sql = ["DELETE FROM " + rosterTable];
      
      for(var i=0; i<list.length; i++)
        inserts.push("('"+list[i]+"', 'default', 1)");

      sqlInsert += inserts.join(',');

      sql.push(sqlInsert);
      sql.push('SELECT * FROM '+rosterTable + ' ORDER BY name');

      connection.query( sql.join(";"), function(err, result) {
        // And done with the connection.
        if(err){
          errCB(err);
        }
        else{
          var rows = result.rows;
          var rosterList = []
          , ros;

          for(var i=0; i<rows.length; i++){
            rosterList.push({id:rows[i].id, name:rows[i].name, status:rows[i].status, isPresent: (rows[i].ispresent == 1)});
          }
          
          callback(rosterList);
        }

        done();
      });
    });
  }
  else
    callback(0);
}

exports.roster = roster;