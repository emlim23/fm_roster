
/*
 * GET settings page.
 */
var config = require('../config');
var dbEnv = require('../lib/dbEnv');

exports.settings = function(req, res){
	res.render('settings', { title: 'FM Roster Settings'});
};

exports.initDB = function(req, res){
	dbEnv.createDB(res);
};