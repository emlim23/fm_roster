
/*
 * GET users listing.
 */
var dbRoster = require('../lib/dbRoster');

exports.list = function(req, res){

	// dbEnv.createDB(res);
	var cb = function(rows){
				res.render('rosterlist', { title: 'FM Roster', employees: rows } );
			}
		, cbErr = function(err){
				res.send(err);
			};

	dbRoster.roster.list(cb, cbErr);
  
};

exports.addnew = function(req, res){

	var r = {id: 0, name:req.body.name, status:"default", isPresent: true}
		, cb = function(id){
				res.send({inserted:id});
			}
		, cbErr = function(err){
				res.send(err);
			};

	dbRoster.roster.save(r, cb, cbErr);
};

exports.delete = function(req, res){

	var cb = function(rows){
				res.send(rows);
			}
		, cbErr = function(err){
				res.send(err);
			};

	dbRoster.roster.delete(req.body.id, cb, cbErr);
};

exports.setNewActive = function(data, cb, errCB){
	dbRoster.roster.setNewActive(data, cb, errCB);
};

exports.setInActive = function(data, cb, errCB){
	dbRoster.roster.setInActive(data, cb, errCB);
};

exports.setPresentStatus = function(data, cb, errCB){
	dbRoster.roster.setPresentStatus(data, cb, errCB);
};

exports.reset = function(data, cb, errCB){
	dbRoster.roster.reset(data, cb, errCB);
};

exports.updatePresent = function(data, cb, errCB){
	dbRoster.roster.updatePresent(data, cb, errCB);
};

exports.updateFromList = function(req, res){
	var list = req.body.list
		, cb = function(rows){
				res.send(rows);
			}
		, cbErr = function(err){
				res.send(err);
			};

	dbRoster.roster.updateFromList(list, cb, cbErr);
};