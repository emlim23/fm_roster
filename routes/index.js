
/*
 * GET home page.
 */
var config = require('../config')
	// , restler = require('restler')
	, dbRoster = require('../lib/dbRoster');

exports.index = function(req, res){
	var cb = function(rows){
			var proto = "https"; //(process.env.NODE_ENV ? "https" : req.protocol);
	  	res.render('index', { title: 'FM Roster', employees: rows, fullURL: proto + "://" + req.get('host')} );
		}
		, cbErr = function(err){
			res.send(err);
		};

	dbRoster.roster.list(cb, cbErr);

	// var getResp = function(url){
	// 	// req.app
	//   restler.get(url).on('complete', function(data){
	//   	// res.send(data);
	//   	var rost = JSON.parse(data);

	//   	if(global.rosterList != null){
	//   		rost = global.rosterList;
	//   	}
	//   	else{
	//   		for(var i=0;i<rost.length; i++){
	//   			rost[i].status = "default";
	//   			rost[i].isPresent = true;
	//   		}

	//   		global.rosterList = rost;
	//   	}

	//   	res.render('index', { title: 'FM Roster', employees: rost, fullURL: req.protocol + "://" + req.get('host')} );
	//   });
	// };

	// getResp(config.dataURL);
};