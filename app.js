
/**
 * Module dependencies.
 */

var express = require('express');
var routes = require('./routes');
var user = require('./routes/user');
var settings = require('./routes/settings');
var http = require('http');
var path = require('path');
var fs = require('fs');

var app = express();
var server = http.createServer(app);
var io = require('socket.io').listen(server);

// global.rosterList = null;

// all environments
app.set('port', process.env.PORT || 8080);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.json());
app.use(express.urlencoded());
app.use(express.methodOverride());
app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));

// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}

app.get('/', routes.index);
app.get('/settings', settings.settings);
app.get('/settings/initdb', settings.initDB);
app.get('/users', user.list);
app.post('/users/addnew', user.addnew);
app.post('/users/delete', user.delete);
app.post('/users/updatelist', user.updateFromList);

io.set('transports', [ 'xhr-polling', 'jsonp-polling', 'htmlfile' ]);

io.sockets.on('connection', function (socket) {
  socket.on('do_emit', function (data) {
  	var returnData = {};
    function emitCB(retData){
      socket.broadcast.emit('update', retData);
    }

    function errCB(err){
      console.log(err);
    }

  	switch(data.event){
  		case 'select': 
  			returnData.event = 'single_update';
  			returnData.data = { id: data.id };

        user.setNewActive(returnData, emitCB, emitCB);
  			break;
      case 'unselect': 
        returnData.event = 'single_update_cancel';
        returnData.data = { id: data.id };

        user.setInActive(returnData, emitCB, emitCB);
        break;
      case 'setPresentStatus': 
        returnData.event = 'present_absent';
        returnData.data = { id: data.id, status: data.status };

        user.setPresentStatus(returnData, emitCB, emitCB);
        break;
  		case 'reset':
  			var list = data.list;
		  	for(var i=0; i<list.length; i++){
		  		list[i].status = "default";
		  	}
  			returnData.event = 'full_refresh';
  			returnData.data = data.list;

        user.reset(returnData, emitCB, emitCB);
  			// global.rosterList = list;

  			break;
  		case 'save':
  			returnData.event = 'refresh_present';
  			returnData.data = data.list;

        user.updatePresent(returnData, emitCB, emitCB);
  			// global.rosterList = saveList;

  			break;
  	}
  });
});

// var chat = io
//   .of('/reset')
//   .on('connection', function (socket) {
//   	socket.on('reset', function (data) {
// 	  	var list = data.list;
// 	  	for(var i=0; i<list.length; i++){
// 	  		list[i].status = "default";
// 	  	}

// 	  	socket.broadcast.emit('full_refresh', { data: list });
// 	  });
//   });

server.listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});
