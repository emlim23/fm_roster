$(function(){
	var $initBtn = $("#initDB")
		, $msg = $("#message div");

	$initBtn.click(function(){
		$msg.text("initializing database...");
		$.ajax({
			url: "/settings/initDB",
			data: {},
			method: "GET",
			complete: function(xhr, stat, err){
				if(err)
					$msg.text(err);
				else
					$msg.text(xhr.responseText);
			}
		})
	});
});