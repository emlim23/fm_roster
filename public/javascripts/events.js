$(function(){
	var socket = io.connect(url, {secure: !isLocal})
		// , reset = io.connect(url + '/reset')
		, $buttons = $("#emp_list button")
		, itemLeft = 0
		, $contextMenu = $(".context-menu");
	
	$buttons.click(buttonClickHandler);

	//RIGHT CLICK
	$buttons.mousedown(function(e){ 
		$contextMenu.data("roster-id", $(this).data("roster-id"));
    if( e.button == 2 ) { 
    	if($(this).hasClass("btn-success")){
				$contextMenu.find(".set-next").hide();
				$contextMenu.find(".set-notdone").show();
			}
			else{
				$contextMenu.find(".set-next").show();
				$contextMenu.find(".set-notdone").hide();
			}			

    	if($(this).hasClass("btn-warning")){
				$contextMenu.find(".set-absent").hide();
				$contextMenu.find(".set-next").hide();
				$contextMenu.find(".set-present").show();
			}
			else{
				$contextMenu.find(".set-absent").show();
				$contextMenu.find(".set-present").hide();
			}

    	$contextMenu.css( {position:"absolute", top:e.pageY, left: e.pageX, display: "block"});
      e.preventDefault(); 
    } 
    return true; 
  }).on('contextmenu', function(){
	  return false;
	}); 

	$contextMenu.find(".set-next").click(function(){
		var rosterID = $contextMenu.data("roster-id");
		socket.emit("do_emit", { event:'select', id: rosterID });

		setButtonState($("#emp_"+rosterID));
	});

	$contextMenu.find(".set-notdone").click(function(){
		var rosterID = $contextMenu.data("roster-id");
		socket.emit("do_emit", { event:'unselect', id: rosterID });

		setButtonToDefault($("#emp_"+rosterID));
	});

	$contextMenu.find(".set-absent").click(function(){
		var rosterID = $contextMenu.data("roster-id");
		socket.emit("do_emit", { event:'setPresentStatus', id: rosterID, status: 0 });

		setButtonPresentAbsent(rosterID, false);
	});

	$contextMenu.find(".set-present").click(function(){
		var rosterID = $contextMenu.data("roster-id");
		socket.emit("do_emit", { event:'setPresentStatus', id: rosterID, status: 1 });

		setButtonPresentAbsent(rosterID, true);
	});

	$(document).click(function(e){
		// var $target = $(e.target);

		// if($target.hasClass("context-menu") || $target.parent().hasClass("context-menu")){

		// }
		// else{
			$contextMenu.hide();
		// }
	})

	//nav bar
	$("#navbar li a").click(function(){
		$(".navbar-toggle").click();
	});

	//Random
	function getRandomInt (min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
	}

	$("#selectRandom").click(function(){
		var $actives = $("#emp_list button.btn-default")
			, randNum = getRandomInt(0, ($actives.length-1))
			, isFirstPass = true;

		itemLeft = $actives.length

		highLighter($actives, randNum);
	});

	function highLighter($actives, randNum){
		setTimeout(function(){
			itemLeft--;
			$actives.removeClass("btn-info");
			$($actives[itemLeft]).addClass("btn-info");

			if(itemLeft>0)
				highLighter($actives, randNum);
			else{
				$actives.removeClass("btn-info");
				$($actives[randNum]).click();
			}
		}, 100);
	}

	function buttonClickHandler(e){
		if($(this).hasClass("btn-default")){
			socket.emit("do_emit", { event:'select', id: $(this).data("roster-id") });

			setButtonState($(this));
		}
		else
			e.preventDefault();
	}

	$("#resetBtn").click(function(){
		if($(this).hasClass("editing")){
			$(".is-present").prop("checked", true);
		}
		else{
			socket.emit("do_emit", { event:'reset', list: empList });
			fullRefresh();
		}
	});

	$("#settingBtn").click(function(){
		if($(this).hasClass("editing")){
			$("#emp_list").show();
			$("#emp_settings").hide();
			$(this).removeClass("editing")
				.find("span").text("Settings");

			saveSettings();
			$("#resetBtn").removeClass("editing");
		}
		else{
			$("#emp_list").hide();
			$("#emp_settings").show();

			$(this).addClass("editing")
				.find("span").text("Save");

			$("#resetBtn").addClass("editing");
		}
	});

	socket.on('update', function (data) {
		switch(data.event){
			case 'single_update': 
  			singleUpdate(data.data.id, true);
  			break;
  		case 'single_update_cancel': 
  			singleUpdate(data.data.id, false);
  			break;
  		case 'present_absent':
  			setButtonPresentAbsent(data.data.id, (data.data.status==1))
  			break;
  		case 'full_refresh':
  			fullRefresh();
  			break;
  		case 'refresh_present':
  			refresh_present(data.data);
  			break;
		}
	  
	});

	function singleUpdate(id, isActive){
		var btn = $("#emp_"+id);

		if(isActive)
	  	setButtonState(btn);
	  else
	  	setButtonToDefault(btn);
	}

	function fullRefresh(){
		$("#emp_list .btn-success").addClass("btn-default").removeClass("active btn-success")
			.find("i.glyphicon").removeClass("glyphicon-refresh spinner glyphicon-ok-circle").addClass("glyphicon-record");
	}

	
	function refresh_present(data){
		var list = data;

		for(var i=0; i<list.length; i++){
			var $empBtn = $("#emp_"+list[i].id)
				, $empChk = $("#emp_present_"+list[i].id);
			if(!list[i].isPresent){
				$empBtn.removeClass("btn-default").addClass("btn-warning")
					.find(".glyphicon").removeClass("glyphicon-refresh spinner glyphicon-ok-circle glyphicon-record").addClass("glyphicon-remove-circle");

				$empChk.removeAttr("checked");
			}
			else{
				if($empBtn.hasClass("btn-warning")){
					$empBtn.addClass("btn-default").removeClass("btn-warning")
						.find("i.glyphicon").removeClass("glyphicon-remove-circle").addClass("glyphicon-record");

					$empChk.attr("checked", true);
				}
			}
		}
	}

	function saveSettings(){
		var empListPresent = [];
		$(".is-present").each(function(){
			empListPresent.push({id:$(this).data("roster-id"), isPresent: $(this).is(":checked")});

		});
		socket.emit("do_emit", { event:'save', list: empListPresent });
		refresh_present(empListPresent);
	}

	function setButtonState(btn) {
		$(".btn-success").removeClass("active");
	  $(".glyphicon-refresh.spinner").removeClass("glyphicon-refresh spinner").addClass("glyphicon-ok-circle");

	  btn.removeClass("btn-default").addClass("btn-success active")
	  	.find("i").removeClass("glyphicon-record glyphicon-ok-circle").addClass("glyphicon-refresh spinner");
	}

	function setButtonToDefault(btn) {
	  btn.removeClass("btn-success active").addClass("btn-default")
	  	.find("i").removeClass("glyphicon-refresh spinner glyphicon-ok-circle").addClass("glyphicon-record");
	}

	function setButtonPresentAbsent(id, isPresent) {
		var btn = $("#emp_"+id)
			, btnClass = "btn-default"
			, glyphIcon = "glyphicon-record";

		if(!isPresent){
			btnClass = "btn-warning";
			glyphIcon = "glyphicon-remove-circle";
			$("#emp_present_"+id).removeAttr("checked");
		}
		else
			$("#emp_present_"+id).attr("checked", true);

	  btn.removeClass("btn-success active btn-default btn-warning").addClass(btnClass)
	  	.find("i").removeClass("glyphicon-refresh spinner glyphicon-ok-circle glyphicon-record glyphicon-remove-circle").addClass(glyphIcon);
	}
});