$(function(){
	var $saveBtn = $("#save")
		, $msg = $("#message")
		, $msg2 = $("#message2")
		, $nameTxt = $("#nameAddNew")
		, $rosterTable = $("#rosterTable")
		, $fetchedTable = $(".spreadsheetTable")
		, $fetchDataBtn = $("#fetchData")
		, $updateBtn = $("#update")
		, rowTmplt = "<div class='row'><div class='col col-xs-10 col-md-10 col-lg-10 emp-name-col'>{{empname}}</div><div class='col col-xs-2 col-md-2 col-lg-2'><button class='btn btn-danger delete-btn btn-xs' data-roster-id={{empid}}><span>Del</span></button></div></div>"
		, rowFetchTmplt = "<div class='row'><div class='col col-xs-10 col-md-10 col-lg-10 emp-name-col'>{{empname}}</div><div class='col col-xs-2 col-md-2 col-lg-2'><input type='checkbox' data-roster-name={{empname}} checked /></div></div>"
		// To see the data that this visualization uses, browse to
		// https://spreadsheets.google.com/ccc?key=tQW2XyL0iNBeq8m6zrFuMRw 
		, ssKey = "0AtNkFxCVRYsOdGxrQUFtN29LLWc2cW9sS2hEMXdfSXc" //"0AtNkFxCVRYsOdElzUnEyMEFUc1B5RnV3SjZLTWhfZkE";
		, visualization
		// SPREADSHEET COLUMNS
		,	col_name = 0
		, col_dept = 1
		, dept_filter = "engineering";


	$updateBtn.attr("disabled", true);

	$saveBtn.click(function(){
		$msg.text("Saving...");
		$.ajax({
			url: "/users/addnew",
			data: {name: $nameTxt.val()},
			method: "POST",
			success: function(xhr, stat, err){
				var newRow = rowTmplt.replace("{{empname}}", $nameTxt.val()).replace("{{empid}}", xhr.inserted);
				$msg.text("Data saved!");

				$(newRow).appendTo($rosterTable).find(".delete-btn").click(deletHandler);
				$nameTxt.val("").focus();

			},
			error: function(obj, err, msg){
				$msg.text(msg);
			}
		});
	});

	$(".delete-btn").click(deletHandler);

	$fetchDataBtn.click(function(){
		$fetchedTable.children().remove();
		$msg2.text("Fetching data...");
		$updateBtn.attr("disabled", true);
		drawDynamicSelect();
	});

	$updateBtn.click(function(){
		var list = [];
		$msg2.text("Posting data...");
		
		$fetchedTable.find(".row").each(function(){
			var $this = $(this);
			if($this.find("input").is(":checked"))
				list.push($this.find(".emp-name-col").text());
		});
		
		if(list.length > 0 && confirm("Warning! This action will delete the current roster and replace it with the list from the spreadsheet. Are you sure you want to continue?")){
			$rosterTable.find(".row:not(.header)").remove();
			//send data to server
			$.ajax({
				url: "users/updatelist",
				data: {list: list},
				method: "POST",
				success: function(xhr, stat, err){
					var newRow;
					if(xhr && xhr.length > 0){
						for(i=0;i<xhr.length;i++){
							newRow = rowTmplt.replace("{{empname}}", xhr[i].name).replace("{{empid}}", xhr[i].id);

							$(newRow).appendTo($rosterTable).find(".delete-btn").click(deletHandler);		
						}
					}
					
					$msg2.text("Done.");
				},
				error: function(obj, err, msg){
					$msg.text(msg);
				}
			});
		}
		
	});

	function deletHandler(){
		var $this = $(this)
			, ans = confirm("Are you sure you want to delete this item?");

		if(ans){
			$.ajax({
				url: "/users/delete",
				data: {id: $this.data("roster-id")},
				method: "POST",
				success: function(xhr, stat, err){
					$this.parent().parent().remove();
				},
				error: function(obj, err, msg){
					$msg.text(msg);
				}
			});
		}
	}

	// drawDynamicSelect() and handleSelectResponse() are used to draw the dynamic list of question ids
	function drawDynamicSelect(){
		var query = new google.visualization.Query(
		'http://spreadsheets.google.com/tq?key='+ssKey+'&pub=0');
		query.setQuery("SELECT B, G ");
		query.send(handleSelectResponse);
	}

	function handleSelectResponse(response) {
		if (response.isError()) {
		return;
		}

		var data = response.getDataTable();

		var mapdata = new Array();
		for (i=0; i<data.getNumberOfRows(); i++){
			if(data.getValue(i, col_dept) == dept_filter){
				mapdata.push(data.getValue(i, col_name));
			}
		} 

		for(i2=0; i2<mapdata.length; i2++){	
			var newRow = rowFetchTmplt.replace("{{empname}}", mapdata[i2]);

			$(newRow).appendTo($fetchedTable);
		}

		$updateBtn.removeAttr("disabled");

		$msg2.text("Done.");
		//sendData(mapdata);
	}
});